package android.karpm.todolist;

import android.karpm.todolist.R;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class TodoWebFragment extends Fragment{

    public static TodoWebFragment newFragment(String redditUrl){
        Bundle arguments = new Bundle();

        TodoWebFragment fragment = new TodoWebFragment();
        fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView = inflater.inflate(R.layout.fragment_todo_web, container, false);

        WebView webView = (WebView)layoutView.findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient());


        return layoutView;
    }
}
