package android.karpm.todolist;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class TodoListFragment extends Fragment {
    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private MainActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback)activity;
        this.activity = (MainActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_todo_list, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ListPost i1 = new ListPost("Do Dishes", "dfdfd", "ssdfsd", "ssdfsd");
        ListPost i2 = new ListPost("Walk Dogs", "dfdfd", "ssdfsd", "ssdfsd");
        ListPost i3 = new ListPost("Homework", "dfdfd", "ssdfsd", "ssdfsd");
        ListPost i4 = new ListPost("Take Trash Out", "dfdfd", "ssdfsd", "ssdfsd");

        activity.todoItems.add(i1);
        activity.todoItems.add(i2);
        activity.todoItems.add(i3);
        activity.todoItems.add(i4);

        TodoAdapter adapter = new TodoAdapter( activityCallback, activity.todoItems);
        recyclerView.setAdapter(adapter);

        return view;
    }
}
