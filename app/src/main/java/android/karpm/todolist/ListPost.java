package android.karpm.todolist;

public class ListPost {
    public String title;
    public String category;
    public String startDate;
    public String dueDate;

    public ListPost(String title, String category, String startDate, String dueDate){
        this.title = title;
        this.category = category;
        this.startDate = startDate;
        this.dueDate = dueDate;
    }
}
