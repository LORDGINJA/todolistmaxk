package android.karpm.todolist;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class TodoAdapter extends RecyclerView.Adapter<TodoHolder>{

    private ArrayList<ListPost> ListPosts;

    public TodoAdapter(ActivityCallback activityCallback, ArrayList<ListPost> ListPosts){
        this.ListPosts = ListPosts;
    }

    @Override
    public TodoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new TodoHolder(view);
    }



    @Override
    public void onBindViewHolder(TodoHolder holder, int i) {
        holder.titleText.setText(ListPosts.get(i).title);
    }

    @Override
     public int getItemCount() {

        return ListPosts.size();
    }
}
